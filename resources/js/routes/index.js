import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);


import welcome from '../components/ExampleComponent'

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', component: welcome, name: 'welcome' },

    ]
});

